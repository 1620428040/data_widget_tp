<?php
$field_definition = [
    "code"=>"",//field_code不能重复，作为默认的字段名、别名
    "name"=>"",
    "table"=>"",//table_code
    "alias"=>"",
    "default"=>"",//省缺值
];
$table_definition = [
    "code" => "",//table_code不能重复，作为默认的表名、别名
    "prefix"=>"",//前缀，比如`cmf_`，没有为空
    "table"=>"",//表名，如果前缀不为空，实际使用时会加上前缀
    "alias"=>"",
    "columns"=>[
        "[field_code]"
    ],
    "pseudos"=>[//伪字段
        "[field_code]"=>[//统计结果作为字段输出
            "sql"=>"[field_code] + [field_code] * 2",//一段获取字段值的SQL表达式
        ],
    ],
];
/**
 * 定义数据表间的关系
 * 可以表述为:[form] [auxiliary] [verb] [numeral] [to] on [on]
 */
$relation_definition = [
    "code"=>"",
    "from"=>"[table_code]",
    "form_relation"=>"[relation_code]",//如果设置[relation_code]，则[table_code]可以为空，此时表述的是之前的关系形成的视图与[to]的关系
    "to"=>"[table_code]",
    "auxiliary"=>"",//助动词:`must`/`maybe`/`should`
    "verb"=>"",//动词:`has`/`belong`,`has`表示外键在[to]中，`belong`表示外键在[from]中
    "numeral"=>"one",//数词:`one`/`many`/[number]
    "on"=>"[from].[field] = [to].[field]",//条件从句
    "statistics"=>[//统计字段，仅一对多关系中存在，其中的字段可以作为正常字段使用；一对一或多对一关系中，所有字段都可以作为正常字段使用
        "[field_code]"=>[//统计结果作为字段输出
            "method"=>"count",//统计方法
            "field"=>"[field_code]",//被统计字段
            "default"=>"",//省缺值
        ],
    ],
];
$data_view_definition = [
    "code"=>"",//视图
    "base"=>"[table_code]",//主表
    "relation"=>[
        "[relation_code]",//通过关系推算其他表
    ],
    "fields"=>[//字段映射表，为空的话映射所有字段，如果字段名与主表重复，自动加前缀`[table_code]_`
        "[field_code]"=>"[field_definition]",
        //如果关系为一对多,则需要进行分组操作，具体返回什么值还需要考虑
    ],
    "field_groups"=>[
        "[group_code]"=>["field_code"],
    ],
];
