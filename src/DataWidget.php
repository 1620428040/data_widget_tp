<?php
namespace DataWidgetTp;

use DataWidget\widget\ListWidget;
use DataWidget\widget\FormWidget;
use DataWidget\widget\FilterWidget;

/** 外观模式 */
final class DataWidget
{
    static private $map;
    static private $fields;
    static private $filters;
    static public function config($map, $fields, $filters)
    {
        static::$map = include $map;
        static::$fields = $fields;
        static::$filters = $filters;
    }
    static public function getList()
    {
        return ListWidget::alloc()->setMap(static::$map["field"])->loadFields(static::$fields);
    }
    static public function getForm()
    {
        return FormWidget::alloc()->setMap(static::$map["field"])->setInputs(static::$fields);
    }
    static public function getFilter()
    {
        return FilterWidget::alloc()->setMap(static::$map["filter"])->setInputs(static::$filters);
    }
}
