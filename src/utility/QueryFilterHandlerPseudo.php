<?php
namespace DataWidgetTp\utility;

use DataWidget\contract\FilterHandler;
use think\db\Query;

/**
 * QueryFilterHandler的接口
 * 其实没有任何方法，也不会调用到。实际上调用的都是QueryFilterHandler
 * 这个类只是为了方便显示接口
 * 毕竟QueryFilterHandler只是个装饰器类，不能继承Query，但Query又没有定义接口
 */
abstract class QueryFilterHandlerPseudo extends Query implements FilterHandler {}
